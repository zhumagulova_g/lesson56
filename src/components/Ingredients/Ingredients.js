import React from 'react';

const Ingredients = (props) => {
    return (
        <div className="ingContainer">
            <button className="ingBtn" type="submit"
                    onClick={props.addIngredient}>Add</button>
            <span>{props.name}</span>
            <span>{props.price} KGS x {props.count}</span>
            <button className="ingBtn"
                    onClick={props.removeIngredient}>Delete</button>
        </div>
    );
};

export default Ingredients;