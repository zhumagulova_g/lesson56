import React, {useState} from 'react';
import './App.css';
import {nanoid} from 'nanoid';
import Ingredients from '../components/Ingredients/Ingredients'
import Burger from '../components/Burger/Burger'

const App = () => {
    const [ingred, setIngred] = useState( [
        {id: nanoid(), name: 'Meat', price: 50, image: 'Meat', count: 0},
        {id: nanoid(), name: 'Cheese', price: 20, image: 'Cheese', count: 0},
        {id: nanoid(), name: 'Salad', price: 5, image: 'Salad', count: 0},
        {id: nanoid(), name: 'Bacon', price: 30, image: 'Bacon', count: 0},

    ]);

  const [ingredients, setIngredients] = useState([]);

  const addIngredient = (e, index, id) => {
    e.preventDefault();

    setIngredients([
        ...ingredients,
        ingred[index],
    ]);

    const elem = ingred.findIndex(ing => ing.id === id );
    const ingCopy = {...ingred[elem]};
    ingCopy.count++;

    const ingredCopy = [...ingred];
    ingredCopy[elem] = ingCopy;
    setIngred(ingredCopy);
    console.log(ingred);
  };

  const removeIngredient = id => {
      const index = ingredients.findIndex(i => i.id === id);
      const ingredientsCopy = [...ingredients];
      ingredientsCopy.splice(index, 1);
      setIngredients(ingredientsCopy);
  };

  const totalSpent = () => {
    return ingredients.reduce((acc, ing) => {
        return acc + ing.price;
      }, 20)
  };



  return (
      <div className="container">
          <div className="box">
              Ingredients
              {ingred.map((ing, index) => (
                  <Ingredients key={ing.id}
                        name={ing.name}
                        price={ing.price}
                        id={ing.id}
                        count={ing.count}
                        addIngredient={(e) => {addIngredient(e, index)}}
                        removeIngredient = {() => removeIngredient(ing.id)}

                  />
              ))}
          </div>
          <div className="box">
              Burger
              <div className="Burger">
                  <div className="BreadTop"/>
                  {ingredients.map((ing) => (
                      <Burger key={ing.id}
                          image={ing.image}/>
                  ))}

                  <div className="BreadBottom"/>
              </div>
              <div className='ingContainer'>Price: {totalSpent()} KGS</div>

          </div>
      </div>
  );
};

export default App;